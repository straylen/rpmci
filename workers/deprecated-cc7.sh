COLORS="\e[41m\e[1m\e[97m"
RESET="\e[0m"

echo "  "
echo "  "
echo -e "    ${COLORS}***************************************************${RESET}"
echo -e "    ${COLORS}**        /!\ End of support for CC7 /!\         **${RESET}"
echo -e "    ${COLORS}**                                               **${RESET}"
echo -e "    ${COLORS}** As of July 1st 2024, CentOS 7 will no longer  **${RESET}"
echo -e "    ${COLORS}** be supported. Please stop using this image.   **${RESET}"
echo -e "    ${COLORS}**                                               **${RESET}"
echo -e "    ${COLORS}**            More info in OTG0145248.           **${RESET}"
echo -e "    ${COLORS}**                                               **${RESET}"
echo -e "    ${COLORS}***************************************************${RESET}"
echo "  "
echo "  "

unset BASH_ENV
