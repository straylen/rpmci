COLORS="\e[41m\e[1m\e[97m"
RESET="\e[0m"

echo "  "
echo "  "
echo -e "    ${COLORS}******************************************************************${RESET}"
echo -e "    ${COLORS}**                   /!\ Fake RHEL Image /!\                    **${RESET}"
echo -e "    ${COLORS}**                                                              **${RESET}"
echo -e "    ${COLORS}** Note that this is not a real Red Hat Enterprise Linux        **${RESET}"
echo -e "    ${COLORS}** docker image, it is really AlmaLinux pretending to be RHEL.  **${RESET}"
echo -e "    ${COLORS}** It should be used only for RPMCI, and is not guaranteed to   **${RESET}"
echo -e "    ${COLORS}** work. If you encounter issues, please let us know.           **${RESET}"
echo -e "    ${COLORS}**                                                              **${RESET}"
echo -e "    ${COLORS}******************************************************************${RESET}"
echo "  "
echo "  "

unset BASH_ENV
